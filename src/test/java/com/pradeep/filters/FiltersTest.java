package com.pradeep.filters;

import com.pradeep.TestHelper;
import com.pradeep.models.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static com.pradeep.utils.Constants.DEFAULT_FILTERS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FiltersTest {

    private Filters filters;

    @Before
    public void setUp() {
        filters = new Filters(DEFAULT_FILTERS);
    }

    @Test
    public void shouldReturnTheConsumer() {
        Customer c = TestHelper.customerWithIn100km().build();

        Optional<Customer> customer = filters.applyFilters(c);

        assertTrue(customer.isPresent());
        assertEquals(customer.get(), c);

    }

    @Test
    public void shouldNotReturnTheConsumer() {
        Customer c = TestHelper.customerOutside100km().build();

        Optional<Customer> customer = filters.applyFilters(c);

        assertFalse(customer.isPresent());
    }

}
