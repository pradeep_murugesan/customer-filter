package com.pradeep.filters;

import com.pradeep.TestHelper;
import com.pradeep.models.Customer;
import com.pradeep.models.Location;
import com.pradeep.utils.DistanceCalculatorInKm;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static com.pradeep.utils.Constants.DEFAULT_FILTERS;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class DistanceFilterTest {

    private DistanceFilter distanceFilter;
    private DistanceCalculatorInKm calculator;

    @Before
    public void setUp() {
        calculator = Mockito.mock(DistanceCalculatorInKm.class);
        distanceFilter = new DistanceFilter(calculator);
    }

    @Test
    public void shouldReturnEmpty() {
        Customer customer = TestHelper.customerOutside100km().build();
        when(calculator.distance(any(Location.class), any(Location.class))).thenReturn(130D);

        Optional<Customer> result = distanceFilter.filter(customer, DEFAULT_FILTERS);

        assertFalse(result.isPresent());

    }

    @Test
    public void shouldReturnTheCustomer() {
        Customer customer = TestHelper.customerWithIn100km().build();
        when(calculator.distance(any(Location.class), any(Location.class))).thenReturn(90D);

        Optional<Customer> result = distanceFilter.filter(customer, DEFAULT_FILTERS);

        assertTrue(result.isPresent());

    }
}
