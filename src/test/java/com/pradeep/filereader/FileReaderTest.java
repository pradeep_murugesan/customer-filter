package com.pradeep.filereader;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import static com.pradeep.utils.Constants.FILE_NAME;
import static org.junit.Assert.assertNotNull;

public class FileReaderTest {

    private FileReader fileReader;

    @Before
    public void setUp() {
        fileReader = new FileReader();
    }

    @Test
    public void shouldReadSuccessFully() throws FileNotFoundException {

        BufferedReader reader = fileReader.getReader(FILE_NAME);
        assertNotNull(reader);

    }

    @Test(expected = FileNotFoundException.class)
    public void shouldThrowExceptionIfNotAbleToRead() throws IOException {
        fileReader.getReader("invalid file");

    }
}
