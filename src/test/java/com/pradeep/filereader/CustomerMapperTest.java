package com.pradeep.filereader;

import com.pradeep.models.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CustomerMapperTest {

    private CustomerMapper customerMapper;

    @Before
    public void setUp() {
        customerMapper = new CustomerMapper();
    }

    @Test
    public void invalidJson() {
        String json = "{\"latitude\": \"52.833502\", \"user_id\": 25";

        Optional<Customer> customer = customerMapper.map(json);

        assertFalse(customer.isPresent());
    }

    @Test
    public void emptyName() {
        String json = "{\"latitude\": \"52.986375\", \"user_id\": 12, \"name\": \"\", \"longitude\": \"-6.043701\"}";

        Optional<Customer> customer = customerMapper.map(json);

        assertFalse(customer.isPresent());
    }

    @Test
    public void nameIsNull() {
        String json = "{\"latitude\": \"52.986375\", \"user_id\": 12, \"longitude\": \"-6.043701\"}";

        Optional<Customer> customer = customerMapper.map(json);

        assertFalse(customer.isPresent());
    }

    @Test
    public void latitudeIsNull() {
        String json = "{\"user_id\": 12,\"name\": \"Christina McArdle\", \"longitude\": \"-6.043701\"}";

        Optional<Customer> customer = customerMapper.map(json);

        assertFalse(customer.isPresent());
    }

    @Test
    public void longitudeIsNull() {
        String json = "{\"latitude\": \"52.986375\", \"user_id\": 12, \"name\": \"Christina McArdle\"}";

        Optional<Customer> customer = customerMapper.map(json);

        assertFalse(customer.isPresent());
    }

    @Test
    public void userIdIsNull() {
        String json = "{\"latitude\": \"52.986375\",\"name\": \"Christina McArdle\", \"longitude\": \"-6.043701\"}";

        Optional<Customer> customer = customerMapper.map(json);

        assertFalse(customer.isPresent());
    }

    @Test
    public void shouldSuccessFullyMap() {
        String json = "{\"latitude\": \"52.986375\", \"user_id\": 12, \"name\": \"c\", \"longitude\": \"-6.043701\"}";

        Optional<Customer> customer = customerMapper.map(json);

        assertTrue(customer.isPresent());
    }
}
