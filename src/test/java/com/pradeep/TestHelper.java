package com.pradeep;

import com.pradeep.models.Customer;
import com.pradeep.models.Location;

public class TestHelper {

    public static Customer.CustomerBuilder customerOutside100km() {
        return Customer.builder()
                .latitude(51.92893)
                .longitude(-10.27699)
                .userId(13)
                .name("user_outside");
    }

    public static Customer.CustomerBuilder customerWithIn100km() {
        return Customer.builder()
                .latitude(52.986375)
                .longitude(-6.043701)
                .userId(3)
                .name("user_inside");
    }

    public static Location.LocationBuilder getLocation() {
        return Location.builder()
                .latitude(52.986375)
                .longitude(-6.043701);
    }
}
