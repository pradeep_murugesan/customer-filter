package com.pradeep.utils;

import com.pradeep.TestHelper;
import com.pradeep.models.Location;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.pradeep.utils.Constants.DUBLIN_LOCATION;

public class DistanceCalculatorInKmTest {

    private DistanceCalculatorInKm distanceCalculatorInKm;

    @Before
    public void setUp() {
        distanceCalculatorInKm = new DistanceCalculatorInKm();
    }

    @Test
    public void shouldCalculateTheDistanceBetween2Locations() {
        final double expectedDistance = 41.76872550088835;
        Location destination = TestHelper.getLocation().build();

        Double distance = distanceCalculatorInKm.distance(DUBLIN_LOCATION, destination);

        Assert.assertEquals(distance.doubleValue(), expectedDistance, .0);
    }

}
