package com.pradeep.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CustomerTest {

    @Test
    public void testToString() {
        Customer customer = Customer.builder()
                .userId(12)
                .name("Foobar")
                .latitude(3.3523)
                .latitude(1.235).build();

        assertEquals(customer.toString(), "12 - Foobar");
    }
}
