package com.pradeep;

import com.pradeep.models.Customer;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static com.pradeep.utils.Constants.DEFAULT_FILTERS;
import static com.pradeep.utils.Constants.FILE_NAME;
import static org.junit.Assert.assertEquals;

public class ProcessorTest {

    private Processor processor;

    @Before
    public void setUp() {
        processor = new Processor(DEFAULT_FILTERS, FILE_NAME);
    }

    @Test
    public void testEntireFlow() throws IOException {
        final int expected = 16;

        List<Customer> customers = processor.process();

        assertEquals(customers.size(), expected);
    }

    @Test
    public void shouldNotStopExecutionIfInvalidCustomerDataIsFound() throws IOException {
        final int expected = 1;
        processor = new Processor(DEFAULT_FILTERS, "customersData.txt");

        List<Customer> customers = processor.process();

        assertEquals(customers.size(), expected);
    }

}
