package com.pradeep;

import com.pradeep.filereader.CustomerMapper;
import com.pradeep.filereader.FileReader;
import com.pradeep.filters.Filters;
import com.pradeep.models.Customer;
import com.pradeep.models.FilterOptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Processor {

    private final ArrayList<Customer> customers;
    private final CustomerMapper mapper;
    private final Filters filters;
    private final String fileName;

    public Processor(FilterOptions options, String fileName) {
        mapper = new CustomerMapper();
        customers = new ArrayList<>();
        filters = new Filters(options);
        this.fileName = fileName;
    }


    public List<Customer> process() throws IOException {

        BufferedReader reader = new FileReader().getReader(fileName);

        String line;
        while ((line = reader.readLine()) != null) {
            mapper.map(line)
                    .flatMap(filters::applyFilters)
                    .ifPresent(customers::add);
        }

        return customers;
    }
}
