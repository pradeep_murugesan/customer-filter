package com.pradeep.utils;

import com.pradeep.models.FilterOptions;
import com.pradeep.models.Location;

public class Constants {

    public static final int EARTH_RADIUS_IN_KM = 6371;
    public static final String FILE_NAME = "customers.txt";
    public static final double LATITUDE = 53.339428;
    public static final double LONGITUDE = -6.257664;
    public static final Location DUBLIN_LOCATION = Location.builder()
            .latitude(LATITUDE)
            .longitude(LONGITUDE)
            .build();
    public static final double FILTER_DISTANCE = 100D;
    public static final FilterOptions DEFAULT_FILTERS =  FilterOptions.builder()
                .filterDistance(FILTER_DISTANCE)
                .source(DUBLIN_LOCATION)
                .build();
}
