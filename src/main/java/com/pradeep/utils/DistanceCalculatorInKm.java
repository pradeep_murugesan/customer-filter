package com.pradeep.utils;

import com.pradeep.models.Location;
import lombok.AllArgsConstructor;

import static com.pradeep.utils.Constants.EARTH_RADIUS_IN_KM;

@AllArgsConstructor
public class DistanceCalculatorInKm {

    public Double distance(Location source, Location destination) {
        double x1 = Math.toRadians(source.getLatitude());
        double y1 = Math.toRadians(source.getLongitude());
        double x2 = Math.toRadians(destination.getLatitude());
        double y2 = Math.toRadians(destination.getLongitude());


        double angle = Math.acos(Math.sin(x1) * Math.sin(x2)
                + Math.cos(x1) * Math.cos(x2) * Math.cos(y1 - y2));

        return angle * EARTH_RADIUS_IN_KM;
    }

}
