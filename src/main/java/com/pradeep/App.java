package com.pradeep;

import com.pradeep.models.Customer;

import java.util.Comparator;
import java.util.List;

import static com.pradeep.utils.Constants.DEFAULT_FILTERS;
import static com.pradeep.utils.Constants.FILE_NAME;

public class App {

    public static void main(final String[] args) {

        try {

            Processor processor = new Processor(DEFAULT_FILTERS, FILE_NAME);
            List<Customer> filteredCustomers = processor.process();

            filteredCustomers.sort(Comparator.comparing(Customer::getUserId));
            System.out.println("***************\nRESULTS\n***************");
            filteredCustomers.forEach(System.out::println);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
