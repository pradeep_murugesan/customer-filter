package com.pradeep.filters;

import com.pradeep.models.Customer;
import com.pradeep.models.FilterOptions;
import com.pradeep.utils.DistanceCalculatorInKm;

import java.util.Optional;

public class Filters {

    private final DistanceFilter distanceFilter;
    private final FilterOptions options;

    public Filters(FilterOptions filterOptions) {
        this.options = filterOptions;

        DistanceCalculatorInKm calculator = new DistanceCalculatorInKm();
        distanceFilter = new DistanceFilter(calculator);
    }


    public Optional<Customer> applyFilters(Customer customer) {

         return Optional.of(customer)
                 .flatMap(c -> distanceFilter.filter(c, options));
    }
}
