package com.pradeep.filters;

import com.pradeep.models.Customer;
import com.pradeep.models.FilterOptions;

import java.util.Optional;

public interface Filter {
    Optional<Customer> filter(Customer customer, FilterOptions options);
}
