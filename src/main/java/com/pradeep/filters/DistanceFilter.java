package com.pradeep.filters;

import com.pradeep.models.Customer;
import com.pradeep.models.FilterOptions;
import com.pradeep.models.Location;
import com.pradeep.utils.DistanceCalculatorInKm;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public class DistanceFilter implements Filter {

    private final DistanceCalculatorInKm calculator;

    @Override
    public final Optional<Customer> filter(Customer customer, FilterOptions options) {

        Location customerLocation = Location.builder()
                .latitude(customer.getLatitude())
                .longitude(customer.getLongitude())
                .build();

        Double distance = calculator.distance(options.getSource(), customerLocation);


        if (distance.compareTo(options.getFilterDistance()) < 0) {
            return Optional.of(customer);
        }
        return Optional.empty();
    }
}
