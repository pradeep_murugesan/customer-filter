package com.pradeep.filereader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pradeep.exception.InvalidCustomerException;
import com.pradeep.models.Customer;

import java.io.IOException;
import java.util.Optional;

public class CustomerMapper {

    private final ObjectMapper mapper;

    public CustomerMapper() {
        mapper = new ObjectMapper();
    }
    public Optional<Customer> map(String json) {
        try {
            Customer customer = mapper.readValue(json, Customer.class);
            customer.validate();
            return Optional.of(customer);

        } catch (IOException e) {
            System.out.println("Mapping exception, Skippping the cusomer " + json);
        } catch (InvalidCustomerException e) {
            System.out.println("Invalid customer data " + json);
        }
        return Optional.empty();
    }
}
