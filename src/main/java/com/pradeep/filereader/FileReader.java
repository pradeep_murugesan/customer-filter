package com.pradeep.filereader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileReader {
    public final BufferedReader getReader(final String filename) throws FileNotFoundException {

        InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(filename);

        if (resourceAsStream == null) {
            throw new FileNotFoundException("Could not find file with name " + filename);
        }
        return new BufferedReader(new InputStreamReader(resourceAsStream));
    }
}
