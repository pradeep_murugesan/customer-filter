package com.pradeep.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pradeep.exception.InvalidCustomerException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
    @JsonProperty("user_id")
    private Integer userId;
    private String name;
    private Double longitude;
    private Double latitude;


    @Override
    public String toString() {
        return this.userId + " - " + this.getName();
    }

    public boolean validate() throws InvalidCustomerException {
        if (name == null || name == "" || latitude == null || longitude == null || userId == null) {
            throw new InvalidCustomerException();
        }
        return true;
    }
}
