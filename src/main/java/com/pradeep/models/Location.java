package com.pradeep.models;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Location {
    private Double latitude;
    private Double longitude;
}
