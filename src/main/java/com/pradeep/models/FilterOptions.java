package com.pradeep.models;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class FilterOptions {
    private Location source;
    private Double filterDistance;
}
