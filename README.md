##### Customer Filter

Given a text file with customer data, filter the customers within a radius of 100 KM from the dublin office.

##### sample customer data

```
{
  "latitude": "51.92893", 
  "user_id": 1, 
  "name": "Alice Cahill", 
  "longitude": "-10.27699"
}
```

##### Solution

The solution is built as a Java console application. Maven is used 
as build tool.

Java version: JDK 8

##### Build

To build the project, run the following from the project root.

```
mvn clean package
```

##### Running the project

once packaged issue the following command to execute the Main class from the project root

```
java -cp target/distancefilter-1.0-SNAPSHOT.jar com.pradeep.App
```

##### Test

To execute the test issue

```
mvn test
```

from the project root.




